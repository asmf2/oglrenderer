#include "InputHandler.h"

using std::cout;
using std::endl;

bool InputHandler::checkEntries (char* argv[]) {
   //cout << "checkEntries" << endl;
   int sizes [nArgs];
   for (int y=0; y<nArgs; y++) {
   int size=0;
      for (int i=0; argv[y][i] != '\0'; i++) {
         size++;
      }
      sizes [y] = size;
   }
   file_types.push_back ("null");
   for (int y=0; y<nArgs; y++) {
      if (argv[y][sizes[y]-4] == '.' && argv[y][sizes[y]-3] == 't' && argv[y][sizes[y]-2] == 'x' && argv[y][sizes[y]-1] == 't') {
         n_txt++;
         file_types.push_back ("txt");
      }
      else if (argv[y][sizes[y]-4] == '.' && argv[y][sizes[y]-3] == 'm' && argv[y][sizes[y]-2] == 't' && argv[y][sizes[y]-1] == 'l') {
         n_mtl++;
         file_types.push_back ("mtl");
      }
      else if (argv[y][sizes[y]-4] == '.' && argv[y][sizes[y]-3] == 'o' && argv[y][sizes[y]-2] == 'b' && argv[y][sizes[y]-1] == 'j') {
         n_obj++;
         file_types.push_back ("obj");
      }
   }
   cout << "Amounts mtl: " << n_mtl << " Amounts obj: " << n_obj  << " Amounts txt: " << n_txt << endl;
   if (n_txt != 1) {
      cout << "The number of basic scene and image description file, cannot be different than one, as was passed as input. Aborting" << endl;
      return false;
   }
   if (n_mtl>n_obj) {
      cout << "Number of material description files (.mtl files), cannot be greater than the number of object description files (.obj files), as was passed as input. Aborting" << endl;
      return false;
   }
   return true;
}

bool InputHandler::checkNumberArguments () {
   //cout << "checkNumberArguments" << endl;
   if (nArgs<2) {
      cout << "No scene description file or .obj file were passed as arguments. Closing..." << endl;
      return false;
   }
   if (nArgs>5) {
      cout << "More arguments than makes sense were passed. Closing..." << endl;
      return false;
   }
   return true;
}

void InputHandler::swapEntries (char * argv []) {
   //cout << "Beggining of swapEntries" << endl;
   if (nArgs == 3) {
      if (file_types[1] != "txt" ) {
         cout << "first argument is not a .txt file. Swapping arguments 1 and 2 of place" << endl;
         char * tmp = argv[1];
         argv[1] = argv[2];
         argv[2] = tmp;
      }
   }
   else if (nArgs == 4) {
      if (file_types[1] != "txt" ) {
         cout << "First argument is not a .txt file. Swapping arguments 1 and 2 of place" << endl;
         char * tmp = argv[1];
         argv[1] = argv[2];
         argv[2] = tmp;
      }
      if (file_types[2] != "mtl") {
         cout << "Second argument is not a .mtl file. Swapping arguments 2 and 3 of place" << endl;
         char * tmp = argv[2];
         argv[2] = argv[3];
         argv[3] = tmp;
      }
   }
   //cout << "End of swapEntries: " << argv[0] << ' ' << argv[1] << ' ' << argv[2] << ' ' << argv[3] << endl;
}
