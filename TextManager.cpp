#include "TextManager.h"

TextManager::TextManager (const char * fileName, bool choice) : FileManager (fileName, choice) {
    fractionaryTenPowers[0] = 0.1, fractionaryTenPowers[1] = 0.01, fractionaryTenPowers[2] = 0.001;
    fractionaryTenPowers[3] = 0.0001, fractionaryTenPowers[4] = 0.00001, fractionaryTenPowers[5] = 0.000001;
    n_threads=1;
}

void TextManager::scanObjFile (Image &image, Scene &scene) {
	cout << "TextManager::scanObjFile" << endl;
	int vec_ctr=0;
	int face_ctr=0;
	string mat_name;
	int ctr=0;
	while (file_reader.good()) {
		getline (file_reader,line);
		if (line[0] == 'v' && line[1] != 'n')
		   getLineValue2 (scene,vec_ctr);
    	else if (line[0] == 'f')
         getLineValue3 (scene,mat_name);
    	else if (line[0] == 'u' && line[1] == 's')
         mat_name = getMtlMaterialName ();
   }
}

void TextManager::scanFile (Image &image, Scene &scene, int value) {
	cout << "TextManager::scanFile" << endl;
	int choice = -1;
	string mat_name;
	while (file_reader.good()) {
		getline (file_reader,line);
		if (line == "Threads") {
			choice=0;
			getData (image,scene,choice);
    	}
		else if (line == "Image") {
			choice=1;
			getData (image,scene,choice);
    	}
    	else if (line == "Camera") {
      	choice=2;
      	getData (image,scene,choice);
    	}
    	else if (line == "Material") {
      	choice=3;
      	getData (image,scene,choice);
    	}
    	else if (line == "Light") {
      	choice=4;
      	getData (image,scene,choice);
    	}
    	else if (line == "Triangle") {
      	choice=5;
      	getData (image,scene,choice);
    	}
    	else if (line [0] == 'n' && line [1] == 'e') {
    	   mat_name = getMtlMaterialName ();
         getMtlData (image,scene,mat_name);
    	}
   }
}

void TextManager::getData (Image &image, Scene &scene, int choice) {
	//cout << "getData" << endl;
	int lineNumbersIndex=0;
  	float lineNumbers [100];
  	while (line != "") {
		getline (file_reader,line);
    	if (line[0] != ' ')
      	continue;
      getLineValue (lineNumbers, lineNumbersIndex);
  	}
  	//cout << "line: " << line << endl;
  	//cout << "Linenumbers: " << lineNumbers [0] << ' ' << lineNumbers [1] << ' ' << lineNumbers [2] << ' ' << lineNumbers [3] <<
  	//' ' << lineNumbers [4] << endl;
  	switch (choice) {
  	   case 0:
      	n_threads = lineNumbers[0];
      	//cout << "n_threads it is: " << n_threads << endl;
      	break;
    	case 1:
      	image.insertData (lineNumbers[0],lineNumbers[1],lineNumbers[2]);
      	break;
    	case 2:
      	scene.setCamera (lineNumbers);
      	break;
    	case 3:
      	scene.setMaterials (lineNumbers);
      	break;
    	case 4:
      	scene.setLights (lineNumbers);
      	break;
    	case 5:
      	scene.setTriangles (lineNumbers);
      	break;
   }
}

void TextManager::getMtlData (Image &image, Scene &scene, string& mat_name) {
   //cout << "getMtlData" << endl;
  	float line_numbers [3];
  	Mtl mtl;
  	string new_line;
  	while (line != ""  && line != "#") {
  	   getline (file_reader,line);
  	   if (line[0] == '\r' || line[0] == '\n')
         break;
      getLineValue4 (line_numbers);
      new_line = getMaterialField (line);
      scene.insertMtlData (mtl,new_line,line_numbers);
  	}
  	scene.insertMtlMaterial (mat_name,mtl);
}

string TextManager::getMaterialField (string& line) {
   //cout << "getMaterialField" << endl;
   string new_line;
   for (int i=0; i<line.size(); i++)
      if ((line [i] >= 'A' && line[i]  <= 'Z') || (line[i] >= 'a' && line[i] <= 'z')) {
         new_line += line[i];
      }
   //cout << "new_line: " << new_line << endl;
   return new_line;
}

string TextManager::getMtlMaterialName () {
   //cout << "getMtlMaterialName" << endl;
   string mat_name;
   int i=0;
   while (line[i] != ' ')
      i++;
   i++;
   for (; i<line.size(); i++) {
      mat_name+= line[i];
   }
   //cout << "mat_name:" <<  mat_name[0] << endl;
   return mat_name;
}

void TextManager::getLineValue4 (float (&line_numbers) [3]) {
   //cout << "getLineValue4" << endl;
   int line_ctr=0, frac_ctr=0, integer_value=0,number_words=0, line_numbers_index=0;
   float float_value=0;
   bool fractionary=false, start=false;
   while (line_ctr < line.size()) {
      if (line[line_ctr] >= '0' && line[line_ctr] <= '9' && fractionary) {
         float_value += fractionaryTenPowers[frac_ctr] * (line[line_ctr] - '0');
         //cout << "float_value it is: " << float_value << " line_numbers_index it is " << line_numbers_index <<  endl;
         frac_ctr++;
         line_ctr++;
         continue;
      }
      else if (line[line_ctr] >= '0' && line[line_ctr] <= '9' && !fractionary) {
         integer_value = (integer_value*10) + line[line_ctr] - '0';
         //cout << "integer_value it is: " << integer_value << " line_numbers_index it is " << line_numbers_index <<  endl;
         start=true;
         line_ctr++;
         continue;
      }
      else if (line [line_ctr] == '.') {
         fractionary=true;
         line_ctr++;
         continue;
      }
      else if (line [line_ctr] == ' ' && start) {
         line_numbers [line_numbers_index] = integer_value + float_value;
         number_words++;
         //cout << "line_numbers[" << line_numbers_index << "]it is: " << line_numbers [line_numbers_index] << endl;
         line_numbers_index++;
         if (number_words==3)
            return;
         integer_value=0, float_value=0, frac_ctr=0;
         fractionary=false;
      }
      line_ctr++;
   }
   line_numbers [line_numbers_index] = integer_value + float_value;
   //cout << "line_numbers[" << line_numbers_index << "]it is: " << line_numbers [line_numbers_index] << endl;
   line_numbers_index++;
   //cout << "Ending getLine4" << endl;
}

void TextManager::getLineValue (float (&lineNumbers) [100], int &lineNumbersIndex) {
  //cout << "getLineValue" << endl;
  int i=0;
  bool negativo = false;
  while (line[i] != ';') {
    //cout << "line.size (): " << line.size () << endl;
    float integerValue=0, floatValue=0;
    while (line[i] != '.' && line[i] != ';') {
      if (line[i] >= '0' && line[i] <= '9')
        integerValue = 10*integerValue + line[i] - '0';
      else if (line[i] == '-')
        negativo = true;
      i++;
    }
    if (negativo) {
      integerValue *= -1;
      negativo = false;
    }
    for (int y=0; line[i] != ';'; i++) {
      if (line[i] == ',') {
        i++;
        break;
      }
      if (line[i] >= '0' && line[i] <= '9') {
        floatValue += (line[i] - '0') *fractionaryTenPowers[y];
        y++;
      }
    }
    lineNumbers[lineNumbersIndex] = integerValue+floatValue;
    //cout << "The 'lineNumbers[lineNumbersIndex]' value it is: " << lineNumbers[lineNumbersIndex] << " and the lineNumbersIndex it is: " << lineNumbersIndex  << endl;
    //cout << "The i value it is: " << i << endl;
    lineNumbersIndex++;
  }
}

void TextManager::getLineValue2 (Scene &scene, int &vec_ctr) {
   //cout << "getLineValue2" << endl;
   int y=0;
   int line_ctr=2;
   //cout << "line char it is: " << line[line_ctr] << endl;
   //cout << "line.size (): " << line.size () << endl;
   //cout << "line it is: << " << line << endl;
   int negative=1;
   bool fractionary=false;
   float integer_value=0, float_value=0;
   int ctr=0;
   float tmp [3];
   while (line_ctr < line.size()) {
      if (line[line_ctr] >= '0' && line[line_ctr] <= '9' && fractionary) {
         float_value += (line[line_ctr] - '0') *fractionaryTenPowers[y];
         //cout << "line[" << line_ctr << "] it is: " << line[line_ctr] << endl;
         //cout << "float_value it is: " << float_value << endl;
         y++;
         line_ctr++;
         continue;
      }
      else if (line[line_ctr] >= '0' && line[line_ctr] <= '9' && !fractionary) {
         integer_value = 10*integer_value + line[line_ctr] - '0';
         //cout << "line[" << line_ctr << "] it is: " << line[line_ctr] << endl;
         //cout << "integer_value it is: " << integer_value << endl;
         line_ctr++;
         continue;
      }
      else if (line [line_ctr] == ' ' && fractionary) {
         //cout << "integer_value it is: " << integer_value << endl;
         //cout << "float_value it is: " << float_value << endl;
         //cout << "line[" << line_ctr << "] it is: " << line[line_ctr] << endl;
         //cout << "integer_value+float_value)*negative it is: " << (integer_value+float_value)*negative << endl;
         tmp [ctr] = ((integer_value+float_value)*negative);
         //cout << "tmp[" << ctr << "] it is: " << tmp[ctr] << endl;
         //cout << "" << endl;
         negative=1;
         fractionary=false;
         integer_value=0;
         float_value=0;
         y=0;
         ctr++;
         line_ctr++;
         continue;
      }
      else if (line [line_ctr] == '-') {
         negative=-1;
         //cout << "line_ctr value it is: " << line_ctr << endl;
         line_ctr++;
         continue;
      }
      else if (line [line_ctr] == '.') {
         fractionary=true;
         //cout << "line_ctr value it is: " << line_ctr << endl;
         line_ctr++;
         continue;
      }
         //cout << "line_ctr value it is: " << line_ctr << endl;
         line_ctr++;
   }

      tmp [ctr] = ((integer_value+float_value)*negative);
      //cout << "tmp[" << ctr << "] it is: " << tmp[ctr] << endl;
      Vec3f temp (tmp[0], tmp[1], tmp[2]);
      scene.vertices.push_back (temp);
      //cout << "integer_value it is: " << integer_value << endl;
      //cout << "float_value it is: " << float_value << endl;
      //cout << "integer_value+float_value)*negative it is: " << (integer_value+float_value)*negative << endl;
      //cout << "vertices[" << vec_ctr << "] it is: " << vertices[vec_ctr].x << ' ' << vertices[vec_ctr].y << ' ' <<
     // vertices[vec_ctr].z << endl;
      vec_ctr++;
}

void TextManager::getLineValue3 (Scene &scene, string& mat_name) {
   //cout << "getLineValue3" << endl;
   bool store_vert=false, bar=false;
   int line_ctr=2;
   vector <int> faces;
   int face_ctr=0, n_faces;
   //cout << "line char it is: " << line[line_ctr] << endl;
   //cout << "line.size (): " << line.size () << endl;
   //cout << "line: " << line << endl;
   float integer_value=0;
   while (line_ctr < line.size()) {
      if (line[line_ctr] >= '0' && line[line_ctr] <= '9' && !bar) {
         integer_value = 10*integer_value + line[line_ctr] - '0';
         //cout << "line[" << line_ctr << "] it is: " << line[line_ctr] << endl;
         //cout << "integer_value it is: " << integer_value << endl;
         store_vert=true;
         line_ctr++;
         continue;
      }
      if (line[line_ctr] == '/')
         bar=true;
      else if (line [line_ctr] == ' ' && store_vert) {
         faces.push_back (integer_value);
         scene.faces_container.push_back (integer_value);
         //cout << "integer_value it is: " << integer_value << endl;
         //cout << "line[" << line_ctr << "] it is: " << line[line_ctr] << endl;
         //cout << "faces[" << face_ctr << "] it is: " << faces[face_ctr] << endl;
         //cout << "" << endl;
         face_ctr++;
         n_faces++;
         integer_value=0;
         store_vert=false, bar=false;
         line_ctr++;
         continue;
      }

         line_ctr++;
   }
      //cout << "integer_value it is: " << integer_value << endl;
      if (store_vert) {
         faces.push_back (integer_value);
         scene.faces_container.push_back (integer_value);
         //cout << "faces[" << face_ctr << "] it is: " << faces[face_ctr] << endl;
         face_ctr++;
         n_faces++;
         //cout << "n_faces it is: " << n_faces << endl;
      }
      //exit (-1);
      scene.assembleTriangles (faces,mat_name,file_name);
}
