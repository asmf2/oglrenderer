#ifndef __VEC3F_h
#define __VEC3F_h

#include <cmath>

class Vec3f {
   public:
      inline Vec3f () : x (0), y (0), z (0) {}
      inline Vec3f (float xx, float yy, float zz) : x (xx), y (yy), z (zz)  { }
      inline void set (float a, float b, float c) {x=a,y=b,z=c;}
      inline float getX () const { return x; }
      inline void setX (const float xx) { x = xx; }
      inline float getY () const { return y; }
      inline void setY (float yy) { y = yy; }
      inline float getZ () const { return z; }
      inline void setZ (float zz) { z = zz; }
      inline void cross (const Vec3f& vec1, const Vec3f& vec2) {
         x = vec1.y * vec2.z - vec1.z * vec2.y;
         y = vec1.z * vec2.x - vec1.x * vec2.z;
         z = vec1.x * vec2.y - vec1.y * vec2.x;
      }
      inline void normalize ()  {
         float inv_length = 1/sqrtf (x*x + y*y + z*z);
         x = x * inv_length, y = y * inv_length, z = z * inv_length;
      }
      inline float operator * (const Vec3f& vec) {
         return x * vec.x + y * vec.y + z * vec.z;
      }
      inline Vec3f operator + (const Vec3f& vec)  {
         Vec3f tmp  (x + vec.x, y + vec.y, z + vec.z);
         return tmp;
      }
      inline Vec3f operator - (const Vec3f& vec) const  {
         Vec3f tmp  (x - vec.x, y - vec.y, z - vec.z);
         return tmp;
      }
      inline void operator - () {
        x = -x, y = -y, z = -z;
      }
   //protected:
      float x,y,z;
};

inline Vec3f operator * (float t, Vec3f& v) {
    Vec3f tmp (v.getX() * t,  v.getY() * t,  v.getZ() * t);
    return tmp;
}

inline Vec3f operator * (Vec3f& v, float t) {
    Vec3f tmp (v.getX() * t,  v.getY() * t,  v.getZ() * t);
    return tmp;
}

#endif
