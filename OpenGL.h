#include <iostream>
#include <stdlib.h>
#include <math.h>
#include "glew.h"
#include "glfw.h"
#include "gl.h"
#include "Miscellaneous.h"
#include "Scene.h"
#include "TGAManager.h"
#include "Vec4f.h"
#include "Matrix4x4.h"


using std::cout;
using std::endl;


void perspectiveGL (GLdouble, GLdouble, GLdouble, GLdouble);
void GLFWCALL windowResize (int, int);
void GLFWCALL handleKeypress (int, int);

class OpenGL {
   public:
      void init (Scene& scene /*,char**/);
      void display (Scene& scene, GLuint programID, GLuint vertexPosition_modelspaceID, GLuint vertex_buffer);
      void perspectiveGL (GLdouble, GLdouble, GLdouble, GLdouble);
      GLuint loadShaders (const char* vs_file_path, const char* fs_file_path);
   private:

};
