#ifndef __IMAGE_h
#define __IMAGE_h

#include <iostream>
#include "Miscellaneous.h"

using std::cout;
using std::endl;

class Image {
  public:
    void insertData (int width, int height, int bpp);
    int getSize () const { return m_size; };
    int getBitsPerPixel () const { return m_bitsPerPixel; };
    inline int getWidth () { return m_width; };
    inline int getHeight () { return m_height; };
  private:
    int m_size;
    int m_bitsPerPixel;
    int m_bytesPerPixel;
    int m_width;
    int m_height;
};

#endif
