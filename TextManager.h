#ifndef TEXTMANAGER_H_INCLUDED
#define TEXTMANAGER_H_INCLUDED

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <cstdlib>
#include "FileManager.h"
#include "Image.h"
#include "Scene.h"
#include "Miscellaneous.h"
#include "Triangle.h"
#include "Vec3f.h"

using std::cout;
using std::endl;

class TextManager : public FileManager
{
    public:
      TextManager (const char *, bool);
      void scanFile (Image&, Scene&, int);
      void scanObjFile (Image &, Scene &);
      void getData (Image &, Scene &, int);
      void getMtlData (Image &, Scene &, string&);
      void getLineValue (float (&) [100], int &);
      void getLineValue2 (
      Scene &scene, int&);
      void getLineValue3 (Scene &, string& mat_name);
      void getLineValue4 (float (&) [3]);
      string getMaterialField (string&);
      string getMtlMaterialName ();
      void getVerticesValues (int, Scene &);
      int getNThreads () {return n_threads ;}
    private:
      std::string line;
      float fractionaryTenPowers [6];
      int n_threads;
};

#endif // TEXTMANAGER_H_INCLUDED
