#ifndef __GEOMETRICOBJECTS_H
#define __GEOMETRICOBJECTS_H
#include <iostream>
#include "Vec3f.h"

#define PI 3.1415926535897932384626433832795

struct Color {
   float red,green,blue;
};

struct Light {
   Vec3f pos;
   Color color;
};

struct Material {
   Color color;
   float reflection;
   float ior;
   float reverseIor;
};

struct Mtl {
   float ns;
   float ni;
   float d;
   float tr;
   Vec3f tf;
   int illum;
   Color ka;
   Color kd;
   Color ks;
   Color ke;
   float reflection;
   std::string material_name;
};

Vec3f convertToRadians (const Vec3f& degrees);

#endif
