#ifndef __MATRIX4x4_h
#define __MATRIX4x4_h

#include "Vec4f.h"

class Matrix4x4 {
   public:
      inline Matrix4x4 ();
      inline void setIdentity ();
      inline void setTranslation (const Vec3f& translation_values);
      inline void setRotation (const Vec3f& rotation_values);
      inline void lookAt (const Vec3f& camera_origin, const Vec3f& camera_target);
      inline void perspective (float fov, float aspect_ratio, float near_clip, float far_clip);
      inline Vec4f operator * (const Vec4f& vec4f) {
         Vec4f tmp;
         tmp.setX (row1.getX() * vec4f.getX() + row1.getY() * vec4f.getY() + row1.getZ() * vec4f.getZ() + row1.getW() * vec4f.getW());
         tmp.setY (row2.getX() * vec4f.getX() + row2.getY() * vec4f.getY() + row2.getZ() * vec4f.getZ() + row2.getW() * vec4f.getW());
         tmp.setZ (row3.getX() * vec4f.getX() + row3.getY() * vec4f.getY() + row3.getZ() * vec4f.getZ() + row3.getW() * vec4f.getW());
         tmp.setW (row4.getX() * vec4f.getX() + row4.getY() * vec4f.getY() + row4.getZ() * vec4f.getZ() + row4.getW() * vec4f.getW());
         return tmp;
      }
      inline Matrix4x4 operator * (const Matrix4x4& matrix4x4) {
         Matrix4x4 tmp;
         tmp.row1.setX (5);
         return tmp;
      }
   //private:
      Vec4f row1, row2, row3, row4;
};

inline Matrix4x4::Matrix4x4 () {
   row1 = Vec4f (0.0f,0.0f,0.0f,0.0f);
   row2 = Vec4f (0.0f,0.0f,0.0f,0.0f);
   row3 = Vec4f (0.0f,0.0f,0.0f,0.0f);
   row4 = Vec4f (0.0f,0.0f,0.0f,0.0f);
}

inline void Matrix4x4::setIdentity () {
   row1 = Vec4f (1.0f,0.0f,0.0f,0.0f);
   row2 = Vec4f (0.0f,1.0f,0.0f,0.0f);
   row3 = Vec4f (0.0f,0.0f,1.0f,0.0f);
   row4 = Vec4f (0.0f,0.0f,0.0f,1.0f);
}

inline void Matrix4x4::setTranslation (const Vec3f& translation_values) {
   row1 = Vec4f (1.0f,0.0f,0.0f,translation_values.getX());
   row2 = Vec4f (0.0f,1.0f,0.0f,translation_values.getY());
   row3 = Vec4f (0.0f,0.0f,1.0f,translation_values.getZ());
   row4 = Vec4f (0.0f,0.0f,0.0f,1.0f);
}

inline void Matrix4x4::setRotation (const Vec3f& rotation_values) {
   float A = cos (rotation_values.getX());
   float B = sin (rotation_values.getX());
   float C = cos (rotation_values.getY());
   float D = sin (rotation_values.getY());
   float E = cos (rotation_values.getZ());
   float F = sin (rotation_values.getZ());
   float AD = A*D;
   float BD = B*D;
   row1 = Vec4f (C*E,-C*F,D,0.0f);
   row2 = Vec4f (BD*E+A*F,-BD*F+A*E,-B*C,0.0f);
   row3 = Vec4f (-AD*E+B*F,AD*F+B*E,A*C,0.0f);
   row4 = Vec4f (0.0f,0.0f,0.0f,1.0f);
}

inline void Matrix4x4::lookAt (const Vec3f &camera_origin, const Vec3f &camera_target) {
   Vec3f foward (camera_origin - camera_target);
   foward.normalize ();
   Vec3f random_vec (0,1,0);
   Vec3f right;
   right.cross (random_vec,foward);
   Vec3f up;
   up.cross (foward,right);
   row1.setX(right.getX()), row1.setY(right.getY()), row1.setZ(right.getZ()), row1.setW(0);
   row2.setX(right.getX()), row2.setY(right.getY()), row2.setZ(right.getZ()), row2.setW(0);
   row3.setX(right.getX()), row3.setY(right.getY()), row3.setZ(right.getZ()), row3.setW(0);
   row4.setX(camera_origin.getX()), row4.setY(camera_origin.getY()), row4.setZ(camera_origin.getZ()), row4.setW(1);
   /*Vec3f random_vec2 (1,0,0);
   Vec3f up2;
   up2.cross (random_vec2,foward);
   Vec3f right2;
   right2.cross (foward,up2);
   cout << "UP: " << up.getX() << ' ' << up.getY() << ' ' << up.getZ() << endl;
   cout << "RIGHT: " << right.getX() << ' ' << right.getY() << ' ' << right.getZ() << endl;
   cout << "UP2: " << up2.getX() << ' ' << up2.getY() << ' ' << up2.getZ() << endl;
   cout << "RIGHT2: " << right2.getX() << ' ' << right2.getY() << ' ' << right2.getZ() << endl;*/
}

inline void Matrix4x4::perspective (float fov, float aspect_ratio, float near_clip, float far_clip) {

}

#endif
