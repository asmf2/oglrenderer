#ifndef TRIANGLE_H_INCLUDED
#define TRIANGLE_H_INCLUDED

#include "Miscellaneous.h"
#include "Vec3f.h"


using std::cout;
using std::endl;

class Triangle {
   public:
      Triangle () {} ;
      Triangle (Vec3f& a, Vec3f& b, Vec3f& c, int id) : vert1 (a), vert2_edge1 (b), vert3_edge2 (c), materialId (id) {}
      int getMaterialId () { return materialId; }
      inline Vec3f getVert1 () {return vert1;}
      inline Vec3f getVert2_edge1 () {return vert2_edge1;}
      inline Vec3f getVert3_edge2 () {return vert3_edge2;}
      inline void setVert2_edge1 (Vec3f v) { vert2_edge1 = v; }
      inline void setVert3_edge2 (Vec3f v) { vert3_edge2 = v; }
      inline bool intersect (Vec3f& camera_dir, Vec3f& camera_ori/*, Vec3f& edge11, Vec3f& edge22*/, Vec3f& dist, Vec3f& s22,
      Triangle& s, float& t, float& u, float& v, int x, int y);
      inline bool generateShadows (Vec3f& light_ray_dir, Vec3f& intersection/*, Vec3f& edge11, Vec3f& edge22*/,Triangle& s, float& t,
       int x, int y);
      inline Vec3f getNormal (int x, int y) { return normal; }
      void setNormal (const Vec3f tmp) { normal = tmp; }
   private:
      Vec3f normal;
      Vec3f vert1,vert2_edge1,vert3_edge2;
      int materialId;
};

inline bool Triangle::generateShadows (Vec3f& light_ray_dir, Vec3f& intersection,/* Vec3f& edge1, Vec3f& edge2,*/ Triangle &s, float &t,
int x, int y) {
   Vec3f s11;
   s11.cross (light_ray_dir,vert3_edge2);
   float divisor =  s11 * vert2_edge1;
   //if (divisor > -0.000001 && divisor < 0.000001)
   if (divisor == 0.0f)
         return false;
   float inv_divisor = 1/divisor;
   Vec3f light_dist = intersection - s.vert1;
   float bary_coord_11 = light_dist * s11 * inv_divisor;
   if (bary_coord_11 < 0.0 || bary_coord_11 > 1.0)
      return false;
   Vec3f s22;
   s22.cross (light_dist, vert2_edge1);
   float bary_coord_22 = light_ray_dir * s22 * inv_divisor;
   if (bary_coord_22 < 0.0 || (bary_coord_11 + bary_coord_22) > 1.0)
      return false;
   float shadow_intersection = vert3_edge2 * s22 * inv_divisor;
   if (0.1f<shadow_intersection && shadow_intersection<t) {
      t=shadow_intersection;
      return true;
   }
   return false;
}

inline bool Triangle::intersect (Vec3f& camera_dir, Vec3f& camera_ori,/* Vec3f& edge1, Vec3f& edge2,*/ Vec3f& dist, Vec3f& s2,
 Triangle &s, float &t, float& u, float& v, int x, int y) {
   //cout << "intersect" << endl;
   Vec3f s1;
   s1.cross (camera_dir,vert3_edge2);
   float det = vert2_edge1 * s1;
   if (det<0.000001)
      return false;
   float bary_coord_1 = dist * s1;
   if (bary_coord_1<0.0 || bary_coord_1>det)
      return false;
   float bary_coord_2 = camera_dir * s2;
   if (bary_coord_2 < 0.0 || (bary_coord_1 + bary_coord_2) > det)
      return false;
   float intersection = vert3_edge2 * s2;
   float invDet = 1/det;
   intersection *= invDet;
   bary_coord_1 *= invDet;
   bary_coord_2 *= invDet;
   if (0.1f<intersection && intersection<t) {
      t=intersection;
      u=bary_coord_1;
      v=bary_coord_2;
      return true;
   }
   return false;
}

#endif
