#include "OpenGL.h"

float rotate_x=0,rotate_y=0,y_location=0,x_location=0;
bool moving_up = false;

void GLFWCALL handleKeypress (int key, int press)  {
   switch (key) {
      case GLFW_KEY_ESC:
         exit(1);
         break;
      case GLFW_KEY_RIGHT:
         rotate_y+=5;
         break;
      case GLFW_KEY_LEFT:
         rotate_y-=5;
         break;
      case GLFW_KEY_UP:
         rotate_x+=5;
         break;
      case GLFW_KEY_DOWN:
         rotate_x-=5;
         break;
      case 'W':
         y_location+=5;
         break;
      case 'S':
         y_location-=5;
         break;
      case 'A':
         x_location-=5;
         break;
      case 'D':
         x_location+=5;
         break;
      /*case GLFW_KEY_HOME:
         if (specular==false) {
            specular=true;
            glMaterialfv (GL_FRONT_AND_BACK,GL_SPECULAR,white_specular_mat);
            glMaterialfv (GL_FRONT_AND_BACK,GL_SHININESS,m_shininess);
         }
         else {
            specular=false;
            glMaterialfv (GL_FRONT_AND_BACK,GL_SPECULAR,blank_material);
            glMaterialfv (GL_FRONT_AND_BACK,GL_SHININESS,blank_material);
         }
         break;
      case GLFW_KEY_END:
         if (diffuse=false) {
            diffuse=true;
            glMaterialfv (GL_FRONT_AND_BACK,GL_DIFFUSE,red_diffuse_mat);
         }
         else {
            diffuse=false;
            glMaterialfv (GL_FRONT_AND_BACK,GL_DIFFUSE,blank_material);
         }
         break;*/
   }
}

void GLFWCALL windowResize (int width, int height) {
   glViewport (0,0,width,height);
   glMatrixMode (GL_PROJECTION);
   glLoadIdentity ();
   perspectiveGL (45.0f,(GLfloat)width/(GLfloat)height,1.0f,1000.0f);
}

void perspectiveGL (GLdouble fov_y, GLdouble aspect, GLdouble z_near, GLdouble z_far) {
   GLdouble fw, fh;
   fh = tan ((fov_y/360)*PI)*z_near;
   fw = fh * aspect;
   glFrustum (-fw,fw,-fh,fh,z_near,z_far);
}

GLuint OpenGL::loadShaders (const char* vertex_file_path, const char* fragment_file_path){
	// Create the shaders
   cout << "loadShaders" << endl;
	// Read the Vertex Shader code from the file
	string vs_instructions;
	FileManager fm (vertex_file_path,false);
   if (fm.isFileOpen()) {
      std::string Line = "";
		while (getline(fm.getFileReader(), Line))
			vs_instructions += "\n" + Line;
	}
	else {
		cout << "Could not open the file" << endl;
		return 0;
	}
	fm.closeFile (false);

	// Read the Fragment Shader code from the file
	string fs_instructions;
	fm.openFile (fragment_file_path);
	if (fm.isFileOpen()) {
		std::string Line = "";
		while (getline(fm.getFileReader(), Line))
			fs_instructions += "\n" + Line;
	}
	else {
		cout << "Could not open the file" << endl;
		return 0;
	}

	GLint result = GL_FALSE;
	int log_length;

	GLuint fs_id = glCreateShader(GL_FRAGMENT_SHADER);
   GLuint vs_id = glCreateShader(GL_VERTEX_SHADER);

	char const * vs_ptr = vs_instructions.c_str();
	glShaderSource (vs_id, 1, &vs_ptr, NULL);
	glCompileShader(vs_id);

	glGetShaderiv (vs_id, GL_COMPILE_STATUS, &result);
	glGetShaderiv (vs_id, GL_INFO_LOG_LENGTH, &log_length);
	if (log_length > 0){
		vector<char> vs_er_ms (log_length+1);
		glGetShaderInfoLog(vs_id, log_length, NULL, &vs_er_ms[0]);
		cout << vs_er_ms[0] << endl;;
	}

	char const * fs_ptr = fs_instructions.c_str();
	glShaderSource (fs_id, 1, &fs_ptr, NULL);
	glCompileShader(fs_id);

	glGetShaderiv(fs_id, GL_COMPILE_STATUS, &result);
	glGetShaderiv(fs_id, GL_INFO_LOG_LENGTH, &log_length);
	if (log_length > 0){
		std::vector<char> fs_er_ms (log_length+1);
		glGetShaderInfoLog(fs_id, log_length, NULL, &fs_er_ms[0]);
		cout << &fs_er_ms[0] << endl;
	}

	GLuint program_id = glCreateProgram();
	glAttachShader (program_id, vs_id);
	glAttachShader (program_id, fs_id);
	glLinkProgram  (program_id);


	glGetProgramiv (program_id, GL_LINK_STATUS, &result);
	glGetProgramiv (program_id, GL_INFO_LOG_LENGTH, &log_length);
	if (log_length > 0){
		std::vector<char> pg_er_ms(log_length+1);
		glGetProgramInfoLog(program_id, log_length, NULL, &pg_er_ms[0]);
		cout << pg_er_ms[0] << endl;
	}

	glDetachShader(program_id, vs_id);
	glDetachShader(program_id, fs_id);

	glDeleteShader(vs_id);
	glDeleteShader(fs_id);

	return program_id;
}


void OpenGL::display (Scene& scene, GLuint programID, GLuint vertexPosition_modelspaceID, GLuint vertex_buffer) {
   glClear (GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);
   glUseProgram (programID);
   glEnableVertexAttribArray (vertexPosition_modelspaceID);
   //glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
   glVertexAttribPointer(
   vertexPosition_modelspaceID, // The attribute we want to configure
      3,                  // size
      GL_FLOAT,           // type
      GL_FALSE,           // normalized?
      0,                  // stride
      (void*)0            // array buffer offset
   );
	glDrawArrays(GL_TRIANGLES, 0, 3); // 3 indices starting at 0 -> 1 triangle
   glDisableVertexAttribArray(vertexPosition_modelspaceID);

   /*Vec3f cam_ori =  scene.getCamera_ori ();

   vector <Mtl> material_container = scene.getMtlContainer();
   int n_triangles = scene.getNTriangles();
   vector <Triangle>& tri_array = scene.getTriangleContainer();*/
   glfwSwapBuffers();
}

void OpenGL::init (Scene& scene /*,char * tex_mem*/) {
   cout << "OpenGL::init" << endl;
   bool running=false;
   /*if (!GLEW_VERSION_2_0) {
      cout << "glew not available. closing..." << endl;
      exit (-1);
   }*/
   glfwInit ();
   glEnable (GL_DEPTH_TEST);
   if (!glfwOpenWindow (1024,768,8,8,8,0,8,0,GLFW_WINDOW)) {
      glfwTerminate();
      exit (1);
   }
   glewInit ();
   Vec3f rotation_values (1.0f,0.0f,0.0f);
   Vec3f translation_values (1.0f,0.0f,0.0f);
   Vec4f vec4f (0.0f,1.0f,0.0f,0.0f);
   Matrix4x4 rotation;
   Matrix4x4 translation;
   Matrix4x4 identity;
   Matrix4x4 view;
   Matrix4x4 projection;
   Matrix4x4 model_view_projection;
   Vec3f camera_origin (4.0f,3.0f,3.0f);
   Vec3f camera_look_at;
   translation.setTranslation (translation_values);
   rotation.setRotation (rotation_values);
   identity.setIdentity ();
   view.lookAt (camera_origin, camera_look_at);
   //exit (-1);
   projection.perspective (45,4.0f/3.0f,0.1,100);
   model_view_projection = projection * view * identity;
   cout << rotation.row1.getX() << ' ' << rotation.row1.getY() << ' ' << rotation.row1.getZ() << ' '  << rotation.row1.getW()  << endl;
   cout << rotation.row2.getX() << ' ' << rotation.row2.getY() << ' ' << rotation.row2.getZ() << ' '  << rotation.row2.getW()  << endl;
   cout << rotation.row3.getX() << ' ' << rotation.row3.getY() << ' ' << rotation.row3.getZ() << ' '  << rotation.row3.getW()  << endl;
   cout << rotation.row4.getX() << ' ' << rotation.row4.getY() << ' ' << rotation.row4.getZ() << ' '  << rotation.row4.getW()  << endl;
   glfwSetWindowTitle ("Test window:");
   glfwSetWindowSizeCallback (windowResize);
   glfwSetKeyCallback (handleKeypress);
   glClearColor (0.0f,0.66f,1.0f,1.0f);

   GLuint program_id = loadShaders ("/media/34GB/demos/Ogl/SimpleVertexShader.vertexshader","/media/34GB/demos/Ogl/SimpleFragmentShader.fragmentshader");
   GLuint matrix_id = glGetUniformLocation (program_id, "MVP");
   glUniformMatrix4fv (matrix_id, 1, GL_TRUE, &model_view_projection.row1.x);
   GLuint vertex_position_modelspace_ID = glGetAttribLocation(program_id, "vertexPosition_modelspace");
   static const GLfloat g_vertex_buffer_data[] = {
		-1.0f, -1.0f, 0.0f,
		 1.0f, -1.0f, 0.0f,
		 0.0f,  1.0f, 0.0f,
   };
   static const GLushort g_element_nuffer_data [] = {0, 1, 2};
   GLuint vertex_buffer;
   glGenBuffers (1, &vertex_buffer);
   glBindBuffer (GL_ARRAY_BUFFER, vertex_buffer);
   glBufferData (GL_ARRAY_BUFFER, sizeof (g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);
   do {
      display (scene,program_id,vertex_position_modelspace_ID,vertex_buffer);
      running = glfwGetWindowParam (GLFW_OPENED);
   } while (running);
   glDeleteBuffers(1, &vertex_buffer);
	glDeleteProgram(program_id);
}
