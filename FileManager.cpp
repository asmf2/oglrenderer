#include "FileManager.h"

FileManager::FileManager (const char * name, bool choice) {
   if (choice)
      file_writer.open (name,ios::binary);
   else
      file_reader.open (name,ios::binary);
}

bool FileManager::isFileOpen () {
    if (file_reader.is_open() || file_writer.is_open()) {
      return true;
    }
    else {
      cout << "The file could not be loaded, please try running the program again" << endl;
      cout << strerror(errno) << endl;
      return false;
    }
}

void FileManager::closeFile (bool choice) {
   if (choice==0) {
      file_reader.close ();
      cout << "file_reader is closing" << endl;
   }
   else {
    file_writer.close ();
    cout << "file_reader is closing" << endl;
   }
}
