#ifndef __VEC4F_H
#define __VEC4F_H

#include "Vec3f.h"

class Vec4f : public Vec3f
{
   public:
      inline Vec4f (): Vec3f(), w(0) {}
      inline Vec4f (float a,float b,float c,float d): Vec3f (a,b,c), w(d) {}
      inline float getW () const {return w;}
      inline void setW (float value) {w=value;}
      inline void translate (const Vec3f& values) {x+values.getX(),y+values.getY(),z+values.getZ();}
   private:
      float w;
};

#endif
