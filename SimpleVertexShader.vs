#version 120

attribute vec3 vertexPosition_modelspace;

uniform mat4 MVP;

void main(){

	//gl_Position = vec4(vertexPosition_modelspace, 1.0);

	gl_Position = MVP * vec4(vertexPosition_modelspace, 1.0);

}

