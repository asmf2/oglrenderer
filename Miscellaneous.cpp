#include "Miscellaneous.h"

Vec3f convertToRadians (const Vec3f& degrees) {
   float x = degrees.getX(), y = degrees.getY(), z = degrees.getZ();
   Vec3f radians (x*(PI/180.0f),y*(PI/180.0f),z*(PI/180.0f));
   return radians;
}
