#include <pthread.h>
#include <cstdlib>
#include <cmath>

#include "TGAManager.h"
#include "TextManager.h"
#include "OpenGL.h"

using std::cout;
using std::endl;
using std::ios;

int main (int argc, char* argv[]) {
   /*InputHandler ih (argc);
   if (!ih.checkNumberArguments ())
      return 0;
   if (!ih.checkEntries (argv))
      return 0;
   ih.swapEntries (argv);*/
   Image image;
   Scene scene (0);
   int n_threads;
   {
      TextManager textManager (argv[1],false);
      if (textManager.isFileOpen()) {
         textManager.storeFilename (argv[1]);
         textManager.scanFile (image, scene,0);
         textManager.closeFile(0);
         n_threads = textManager.getNThreads();
      }
      if (argc==3) {
         textManager.openFile (argv[2]);
         if (textManager.isFileOpen()) {
            textManager.storeFilename (argv[2]);
            textManager.scanObjFile (image, scene);
            textManager.closeFile(0);
            n_threads = textManager.getNThreads();
         }
      }
      if (argc==4) {
         textManager.openFile (argv[2]);
         if (textManager.isFileOpen()) {
            textManager.storeFilename (argv[2]);
            textManager.scanFile (image, scene,1);
            textManager.closeFile(0);
            n_threads = textManager.getNThreads();
         }
         textManager.openFile (argv[3]);
         if (textManager.isFileOpen()) {
            textManager.storeFilename (argv[3]);
            textManager.scanObjFile (image, scene);
            textManager.closeFile(0);
            n_threads = textManager.getNThreads();
         }
      }
   }
   scene.copyMaterials ();
   scene.setTriMem ();
   scene.copyTriangles ();
   vector <Triangle>().swap (scene.getTriangleContainer());
   scene.toWindCW();
   OpenGL gl;
   //har * texture_mem;
   gl.init (scene /*,texture_mem*/);
   scene.unsetTriMem ();
   //delete texture_mem;
   return 0;
}
